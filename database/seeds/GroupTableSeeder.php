<?php

use Illuminate\Database\Seeder;
use App\Group;
// use Faker\Generator as Faker;

class GroupTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        factory(App\Group::class, 20)->create();
       
        //Uzpildymas rankiniu budu:
        // $faker = Faker::create();        

        // foreach (range(1,10) as $index) {
        //     $dateStamp= random_int(1262055681, time());
        //     $dateStamp2= random_int(1262055681, time());
        //     DB::table('group')->insert([
        //         'course_id' => random_int(1, 10),
        //         'lecturer_id' => random_int(1, 5),
        //         'name' => str_random(10),
        //         'from_date' => date($format = 'Y-m-d', $max = $dateStamp),
        //         'to_date' => date($format = 'Y-m-d', $max = $dateStamp2)
        //     ]);
        // }      
           
    }
}
