<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Course;

class CourseController extends Controller
{
    public function show($id, Request $request)
    {
        return view('Course/course_view', ['course' => Course::find($id)]);
    }

    public function edit($id, Request $request)
    {
        return view('course_edit', ['course' => Course::find($id)]);
    }

    public function update($id, Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:32'
        ]);

        $course = Course::find($id);
        $course->name = $request->input('name');
        $course->save();
        redirect(route('course_all'));
    }

    public function all( Request $request)
    {
        return view('Course/course_all', ['courses' => Course::all()]);
    }


    public function create(Request $request)
    {
        if($request->isMethod('post')){
            $this->validate($request, [
                'name' => 'required'
            ]);
            
            $course = new Course;
            $course->name = $request->input('name');
            $course->save();
        }
        return view('Course/course_create');
    }
}
