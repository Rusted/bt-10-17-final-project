<?php
namespace App\Http\Controllers;

use App\Group;
use App\Student;
use App\User;
use Illuminate\Http\Request;

class GroupController extends Controller
{

    function list(Request $request) {
        return view('groups_list', ['groups' => Group::all()]);
    }

    public function create(Request $request)
    {
        if ($request->isMethod('post')) {
            $this->validate($request, [
                'course_id' => 'required',
                'lecturer_id' => 'required',
                'name' => 'required',
                'from_date' => 'required',
                'to_date' => 'required',
            ]);

            $group = new Group;
            $group->course_id = $request->input('course_id');
            $group->lecturer_id = $request->input('lecturer_id');
            $group->name = $request->input('name');
            $group->from_date = $request->input('from_date');
            $group->to_date = $request->input('to_date');
            $group->save();
        }
        return view('Group/create');
    }

    public function show($id, Request $request)
    {
        return view('group_view', ['group' => Group::find($id)]);
    }
    public function viewStudent(Group $group, Request $request)
    {
        return view('Group/view_student', ['group' => $group]);
    }

    public function viewTeacher(Group $group, Request $request)
    {
        return view('Group/view_teacher', [
            'group' => $group,
            'students' => User::findStudents(),
        ]);
    }

    public function allTeacher(Request $request)
    {
        return view('Group/all_teacher', ['groups' => \App\Group::all()]);
    }

    public function edit(Group $group, Request $request)
    {
        if ($request->isMethod('POST')) {
            $this->validate($request, [
                'name' => 'required',
                'course_id' => 'required|exists:course,id'
            ]);

            $group->name = $request->input('name');
            $group->course_id = $request->input('course_id');
            $group->save();

            return redirect('teacher/group/all')->with('success', 'Group updated successfully');
        }

        return view('Group/edit', ['group' => $group, 'courses' => \App\Course::all()]);
    }

    public function delete(Group $group, Request $request)
    {
        if ($request->isMethod('POST')) {
            $group->delete();
            return redirect('teacher/group/all')->with('success', 'Group deleted successfully');
        }

        return redirect('teacher/group/all');
    }

    public function assignStudents(Group $group, Request $request)
    {
        $this->validate($request, [
            'student.*' => 'exists:users,id'
        ]);

        $selectedStudentIds = $request->input('student', []);
        Student::assignStudents($group, array_keys($selectedStudentIds));

        return redirect('teacher/group/' . (int) $group->id)->with('success', 'Group students updated successfully');
    }
}
