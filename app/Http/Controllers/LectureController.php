<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Lecture;

class LectureController extends Controller
{
    public function viewStudent(Lecture $lecture, Request $request)
    {
        return view('Lecture/view_student', ['lecture' => $lecture]);
    }

    public function viewTeacher(Lecture $lecture, Request $request)
    {
        return view('Lecture/view_teacher', ['lecture' => $lecture]);
    }

    public function allTeacher(Request $request)
    {
        return view('Lecture/all_teacher', ['lectures' => \App\Lecture::all()]);
    }

    public function delete(Lecture $lecture, Request $request)
    {
        if ($request->isMethod('POST')) {
            $lecture->delete();
            return redirect('teacher/lecture/all')->with('success', 'lecture deleted successfully');
        }

        return redirect('teacher/lecture/all');
    }
    public function create(Request $request)
    {
        if ($request->isMethod('post')) {
            $this->validate($request, [
                'grupes_id' => 'required',
                'name' => 'required'
            ]);

            $lecture = new Lecture;
            $lecture->grupes_id = $request->input('grupes_id');
            $lecture->name = $request->input('name');
            $lecture->aprasas = $request->input('aprasas');
            $lecture->data = $request->input('data');
            $lecture->save();
        }
        return view('Lecture/create');
    }
}
