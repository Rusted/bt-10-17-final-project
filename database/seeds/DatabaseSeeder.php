<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CourseTableSeeder::class);

        $this->call(LectureTableSeeder::class);

        $this->call(FileTableSeeder::class);

        $this->call(UsersTableSeeder::class);

        $this->call(GroupTableSeeder::class);

        $this->call(StudentTableSeeder::class);
    }
}
