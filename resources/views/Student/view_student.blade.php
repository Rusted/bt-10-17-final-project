@extends('layouts/app')

@section('content')
    <h1>Student {{$user->name}}</h1>
    <p>Email - {{$user->email}}</p>
    <ul>
    @foreach($user->students as $student)
            <li>
                <a href="{{route('student_group', ['group' => $student->group->id])}}">
                    {{$student->group->name}}
                </a>
            </li>
    @endforeach
    </ul>
@endsection
