@extends('layouts/app')

@section('content')
    <h1>Group {{$group->name}}</h1>
    <p>Course name {{$group->course->name}}</p>
    <p>Teacher is {{$group->lecturer->name}}</p>
    <h1>Lectures </h1>
    <ul>
    @foreach($group->lectures as $lecture)
        <li>{{$lecture->name}} {{$lecture->data}}</li>
    @endforeach
    </ul>

    <h1>Students </h1>
    <form method="post" action="{{route('teacher_group_assign_students', ['group' => $group->id])}}">
        <ul>
        @foreach($students as $student)
            <li>
                <input type="checkbox" @if($student->hasStudentGroup($group)) checked @endif value="{{$student->id}}" name="student[{{$student->id}}]" />
                {{$student->name}} {{$student->email}}
            </li>
        @endforeach
        </ul>
        <input type="submit" value="Update students">
        {{csrf_field()}}
    </form>
@endsection
