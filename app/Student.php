<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $table = 'student';

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function group()
    {
        return $this->belongsTo(Group::class);
    }

    public static function assignStudents(Group $group, $studentIds)
    {
        static::where('group_id', $group->id)->delete();

        foreach($studentIds as $studentId) {
            $student = new Student();
            $student->group_id = $group->id;
            $student->user_id = $studentId;
            $student->save();
        }
    }
}
