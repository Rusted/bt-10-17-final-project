@extends('layouts.app')
@section('content')
<a href="{{route('teacher_group_all')}}"><< Atgal</a>
    <h1>{{$group->name}}</h1>
    <form method="POST">
        <div class="form-group @if ($errors->has('name')) has-danger @endif">
            <label class="col-md-2 control-label">Pavadinimas</label>
            <div class="col-md-10">
                @if ($errors->has('name'))
                    <small class="text-danger">
                        {{$errors->first('name')}}
                    </small>
                @endif
                <input type="text" name="name" class="form-control @if ($errors->has('name')) is-invalid @endif"  value="{{ Request::old('name') ?  Request::old('name'): $group->name }}">
            </div>
        </div>

        <div class="form-group @if ($errors->has('name')) has-danger @endif">
            <label class="col-md-2 control-label">Kursas</label>
            <div class="col-md-10">
                @if ($errors->has('course_id'))
                    <small class="text-danger">
                        {{$errors->first('course_id')}}
                    </small>
                @endif
                <select name="course_id">
                    @foreach($courses as $course)
                        <option type="text" value="{{$course->id}}" {{$course->id == $group->course->id ? 'selected': ''}}>{{$course->name}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        {{csrf_field()}}

        <div class="form-group @if ($errors->has('name')) has-error @endif">
            <label class="col-md-2 control-label"></label>
            <div class="col-md-10">
                <input type="submit">
            </div>
        </div>
    </form>
@endsection
