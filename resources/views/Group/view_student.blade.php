@extends('layouts/app')

@section('content')
   <h1>Group {{$group->name}}</h1>
   <p>Course name {{$group->course->name}}</p>
   <p>Teacher is {{$group->lecturer->name}}</p>

   <h1>Lectures </h1>
   <ul>
   @foreach($group->lectures as $lecture)
       <li>
          <a href="{{route('student_lecture', ['lecture' => $lecture->id])}}">
            {{$lecture->name}} {{$lecture->data}}
          </a>
       </li>
   @endforeach
   </ul>

   <h1>Students </h1>
   <ul>
   @foreach($group->students as $student)
       <li>{{$student->user->name}} {{$student->user->email}}</li>
   @endforeach
   </ul>
@endsection
