<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Group::class, function (Faker $faker) {
    $courseId = random_int(1, 10);
    return [
        'course_id' =>$courseId,
        'lecturer_id' => 1,
        'name' =>  substr(\App\Course::find($courseId)->name, 7) . ' ' . $faker->randomLetter . ' ' . $faker->randomNumber(2),
        'from_date' => $faker->date(),   
        'to_date' => $faker->date()  
    ];
});

