<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\App;

class User extends Authenticatable
{
    use Notifiable;

    const TYPE_TEACHER = 1;
    const TYPE_STUDENT = 2;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function groups()
    {
        return $this->hasMany('App\Group');
    }

    public function students()
    {
        return $this->hasMany('App\Student');
    }

    public static function findStudents()
    {
        return static::where('type', self::TYPE_STUDENT)->get();
    }

    public function hasStudentGroup(Group $group): bool
    {
        foreach ($this->students as $student) {
            if ($student->group_id == $group->id) {
                return true;
            }
        }

        return false;
    }

}
