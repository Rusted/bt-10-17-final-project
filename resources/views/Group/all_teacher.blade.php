@extends('layouts.app')

@section('content')
    <h1>All Groups</h1>
    <ul>
        @foreach($groups as $group)
            <a href="{{route('group_edit', ['group' => $group->id])}}"><li> {{$group->name}} </li></a>
            <form action="{{route('group_delete', ['group' => $group->id])}}" method="POST">
                {{csrf_field()}}
                <input type="hidden" value="{{$group->id}}">
                <input type="submit" value="Delete" />
            </form>
        @endforeach
    </ul>
    <hr>
    <h3>Pridėti naują grupę</h3>
    <a href="{{route('group_create')}}">Registruoti naują grupę</a>
@endsection
