<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\File::class, function (Faker $faker) {
    return [
        'lecture_id' => random_int(1, 10),
        'name' => $faker->word . '.' . $faker->fileExtension,
        'path' => 'app/storage/' . $faker->sha256,
    ];
});

