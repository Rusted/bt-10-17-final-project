<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Jonas mokytojas',
            'email' => 'jonas.mokytojas@test.com',
            'id' => 1,
            'type' => App\User::TYPE_TEACHER,
            'password' => Hash::make('123456')
        ]);
        DB::table('users')->insert([
            'name' => 'Lukas studentas',
            'email' => 'lukas.studentas@test.com',
            'id' => 2,
            'type' => App\User::TYPE_STUDENT,
            'password' => Hash::make('123456')
        ]);
        DB::table('users')->insert([
            'name' => 'Domas studentas',
            'email' => 'domas.studentas@test.com',
            'id' => 3,
            'type' => App\User::TYPE_STUDENT,
            'password' => Hash::make('123456')
        ]);
        DB::table('users')->insert([
            'name' => 'Vilius mokytojas',
            'email' => 'vilius.mokytojas@test.com',
            'id' => 4,
            'type' => App\User::TYPE_TEACHER,
            'password' => Hash::make('123456')
        ]);
    }
}
