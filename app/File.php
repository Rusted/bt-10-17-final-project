<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use App\Lecture;

class File extends Model
{
    protected $table ='file';

    public function lecture()
    {
       return $this->belongsTo(Lecture::class);
    }

}
