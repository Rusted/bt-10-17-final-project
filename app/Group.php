<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Lecture;
use App\Course;
use App\Student;

class Group extends Model
{
    protected $table = 'group';

    public function students()
    {
        return $this->hasMany(Student::class);
    }

    public function lectures()
    {
        return $this->hasMany(Lecture::class, 'grupes_id')->orderBy('data');
    }

    public function lecturer()
    {
        return $this->belongsTo(User::class);
    }

    public function course()
    {
        return $this->belongsTo(Course::class);
    }
}
