<?php

Route::get('/', function () {
    return view('welcome');
});


Route::match(['get', 'post'], '/group/create', 'GroupController@create')->name('group_create');
Route::get('/groups', 'GroupController@list')->name('groups_list');
Route::match(['GET', 'POST'], '/course/create', 'CourseController@create')->name('course_create');
Route::match(['GET', 'POST'], '/lecture/create', 'LectureController@create')->name('lecture_create');
Route::get('course/all', 'CourseController@all')->name('course_all');
Route::get('course/{id}', 'CourseController@show')->name('course_view');
Route::get('course/{id}/edit', 'CourseController@edit')->name('course_edit');
Route::post('course/{id}/update', 'CourseController@update')->name('course_update');
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');


Route::group(['middleware' => ['auth']], function () {
    Route::get('/student/student/{user}' , 'StudentController@viewStudent')->name('student_student');
    Route::get('/student/group/{group}', 'GroupController@viewStudent')->name('student_group');
    Route::get('/student/lecture/{lecture}', 'LectureController@viewStudent')->name('student_lecture');    
});


Route::get('/teacher/group/all', 'GroupController@allTeacher')->name('teacher_group_all');
Route::get('/teacher/lecture/all', 'LectureController@allTeacher')->name('teacher_lecture_all');
Route::match(['GET', 'POST'], '/group/{group}/edit', 'GroupController@edit')->name('group_edit');
Route::post('/group/{group}/delete', 'GroupController@delete')->name('group_delete');
Route::post('/lecture/{lecture}/delete', 'LectureController@delete')->name('lecture_delete');
Route::post('/teacher/group/{group}/assignStudents', 'GroupController@assignStudents')->name('teacher_group_assign_students');
Route::get('/teacher/group/{group}', 'GroupController@viewTeacher')->name('teacher_group');
Route::get('/teacher/lecture/{lecture}', 'LectureController@viewTeacher')->name('teacher_lecture');





