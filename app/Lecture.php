<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\File;

class Lecture extends Model
{
    protected $table = 'lecture';

    public function files()
    {
        return $this->hasMany(File::class);
    }
}
