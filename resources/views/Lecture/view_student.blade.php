@extends('layouts/app')

@section('content')
<a href="{{route('student_lecture_all')}}"><< Atgal</a>
    <h1>Lecture {{$lecture->name}} </h1>
    <h3>{{$lecture->data}}</h3>
    <p>{{$lecture->aprasas}}</p>

    <h1>Files</h1>
    <ul>
    @foreach($lecture->files as $file)
        <li> {{$file->name}} </li>
    @endforeach
    </ul>
@endsection
