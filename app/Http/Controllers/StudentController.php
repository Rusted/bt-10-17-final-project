<?php

namespace App\Http\Controllers;
use App\Student;
use App\User;
use Illuminate\Http\Request;

class StudentController extends Controller
{
    public function viewStudent(User $user , Request $request)
    {
    return view('Student/view_student', ['user' => $user]);
    }
}
