<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
 */

$factory->define(App\Lecture::class, function (Faker $faker) {
    return [
        'grupes_id' => rand(1, 10),        
        'name' => $faker->sentence,
        'aprasas' => $faker->paragraph,
        'data' => $faker->dateTimeBetween('now', '+2 month')
    ];
});
