@extends ('layouts/app')

@section('content')
<ul>
<h1>Kursų sąrašas:</h1>
@foreach ($courses as $course)
    <li>
        <a href="{{ route('course_view', ['id' => $course->id]) }}">
            {{ $course->name }}
        </a>
    </li>
@endforeach
</ul>
<hr>
<a href="{{ route('course_create') }}">Naujo kurso registracija</a>

@endsection
