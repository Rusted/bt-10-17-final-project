@extends('layouts.app')

@section('content')
    <h1>Visos paskaitos</h1>
    <ul>
        @foreach($lectures as $lecture)
            <a href="{{route('teacher_lecture', ['lecture' => $lecture->id])}}"><li> {{$lecture->name}} </li></a>
            <form action="{{route('lecture_delete', ['lecture' => $lecture->id])}}" method="POST">
                {{csrf_field()}}
                <input type="hidden" value="{{$lecture->id}}">
                <input type="submit" value="Delete" />
            </form>
        @endforeach
    </ul>
    <hr>
    <h3>Pridėti naują paskaitą</h3>
    <a href="{{route('lecture_create')}}">Registruoti naują paskaitą</a>
    
@endsection
