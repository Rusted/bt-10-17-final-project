<?php

use Illuminate\Database\Seeder;
use App\Student;
use App\Group;
use App\User;
use Illuminate\Support\Facades\DB;

class StudentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('student')->insert([
            'group_id' => 1,
            'user_id' => 2,
        ]);

        DB::table('student')->insert([
            'group_id' => 2,
            'user_id' => 2,
        ]);

        DB::table('student')->insert([
            'group_id' => 2,
            'user_id' => 3,
        ]);

        DB::table('student')->insert([
            'group_id' => 3,
            'user_id' => 3,
        ]);
    }
}
